#!/bin/bash

# Update hosts file
echo "[TASK 1] Update /etc/hosts file"
cat >>/etc/hosts<<EOF
172.42.42.100 kmaster.example.com kmaster
172.42.42.101 kworker1.example.com kworker1
172.42.42.102 kworker2.example.com kworker2
EOF


echo "[TASK 2] Install docker container engine"
apt-get install apt-transport-https ca-certificates curl software-properties-common -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt-get update -y
apt-get install docker-ce -y

# add ccount to the docker group
usermod -aG docker vagrant

# Enable docker service
echo "[TASK 3] Enable and start docker service"
systemctl enable docker >/dev/null 2>&1
systemctl start docker

# Install apt-transport-https pkg
#echo "[TASK 8] Installing apt-transport-https pkg"
#apt-get update && apt-get install -y apt-transport-https curl
#curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -

apt-get update -y

# Enable ssh password authentication
#echo "[TASK 11] Enable ssh password authentication"
#sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config
#systemctl restart sshd

# Update vagrant user's bashrc file
echo "export TERM=xterm" >> /etc/bashrc

echo "[TASK 4] Configure vagrant directories for SSH and Kube config"
mkdir -p /home/vagrant/.ssh
cp /vagrant/id_rsa* /home/vagrant/.ssh/
chmod 700 /home/vagrant/.ssh
chmod 600 /home/vagrant/.ssh/*
mkdir -p /home/vagrant/.kube

echo "[TASK 5] Setup SSH key auth and kube .config"
ssh -o "StrictHostKeyChecking no" -i /home/vagrant/.ssh/id_rsa vagrant@kmaster  "cat .kube/config" > /home/vagrant/.kube/config 
ssh -o "StrictHostKeyChecking no" -i /home/vagrant/.ssh/id_rsa vagrant@kworker1 "ls" 
ssh -o "StrictHostKeyChecking no" -i /home/vagrant/.ssh/id_rsa vagrant@kworker2 "ls" 
cat ~/.ssh/known_hosts > /home/vagrant/.ssh/known_hosts

echo "[TASK 6] Reset vagrant permissions."
chmod 600 /home/vagrant/.kube/config
chown -R vagrant: /home/vagrant

echo "[TASK 7] Install helm"
curl https://baltocdn.com/helm/signing.asc | sudo apt-key add -
sudo apt-get install apt-transport-https --yes
echo "deb https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
sudo apt-get update
sudo apt-get install helm

echo "Becomming vagrant"
su - vagrant
export KUBECONFIG=/home/vagrant/.kube/config

echo "[TASK 8] Running tasks from helm_required.sh"
bash /vagrant/helm_required.sh
if [ -f /vagrant/develop/bootstrap.sh ]; then
    echo "[TASK 9] Running tasks from develop/bootstrap.sh"
    bash /vagrant/develop/bootstrap.sh
else
    echo "[TASK 9] No file found to run at .develop/bootstrap.sh"
fi
