# Vagrant Kubernetes

## Setup

*Requirements:*


- Install vagrant: (https://www.vagrantup.com/)
- Install virtualbox: (https://www.virtualbox.org/)

- Optional: Install helm (https://helm.sh/docs/intro/install/)

The easiest way to install on Linux is to use the package manager:

*Debian based distributions*

> apt-get install vagrant 
> apt-get install virtualbox

*Enterprise linux based ditributions < 8*
> yum install vagrant 
> yum install virtualbox

*Enterprise linux based distributions > 7 and fedora*

> dnf install vagrant 
> dnf install virtualbox

*MAC Systems with Homebrew (https://brew.sh)*

>brew install vagrant
> brew install virtualbox

*Clone the repository:*

> git clone https://frankiedoodles@bitbucket.org/frankiedoodles/k8s_ubuntu.git

*Run up the cluster*

> vagrant up

*Setup your local context*

***NOTE: This will overwrite your existing ~/.kube/config!*** 

backup you existing ~/.kube/config if you want to keep it

> sh ./set_kube_context.sh

If all went well then you should be able to point your browser to http://172.42.42.106/ and see the ingres controller welcome page.

## SSH files

The id_rsa, id_rsa.pub and the authorized_key are used by the develop vagrant user for access to the kubernetes nodes

## Develop directory

The /develop directory has been added to the .gitignore so that it can be used for mounting secret personal files away from the public repository.

## Next Steps:

  * Next steps add auto indtalled nginx ingress controller and metallb via helm charts.
  * Addition of private repository via second git private project mounted at the /develop directory.
  * Test docker build and updates are being handled cottrctly.

## NGINX Ingress Controller

The NGINX configuration may ned adjusting to meet network requirements.

The nginx-ingress controller has been installed.
It may take a few minutes for the LoadBalancer IP to be available.
You can watch the status by running 'kubectl --namespace ingress-nginx get services -o wide -w required-02-nginx-ingress-controller'

An example Ingress that makes use of the controller:

  apiVersion: extensions/v1beta1
  kind: Ingress
  metadata:
    annotations:
      kubernetes.io/ingress.class: nginx
    name: example
    namespace: foo
  spec:
    rules:
      - host: www.example.com
        http:
          paths:
            - backend:
                serviceName: exampleService
                port: 80
              path: /
    # This section is only required if TLS is to be enabled for the Ingress
    tls:
        - hosts:
            - www.example.com
          secretName: example-tls

If TLS is enabled for the Ingress, a Secret containing the certificate and key must also be provided:

  apiVersion: v1
  kind: Secret
  metadata:
    name: example-tls
    namespace: foo
  data:
    tls.crt: <base64 encoded cert>
    tls.key: <base64 encoded key>
  type: kubernetes.io/tls

