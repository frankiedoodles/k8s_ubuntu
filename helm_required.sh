echo "=== HELM REQUIRED ==="
echo "[TASK 01] Install bitnami charts."
helm repo add bitnami https://charts.bitnami.com/bitnami

echo "[TASK 02] Install or upgrade Metallb."
helm upgrade --install required-01 bitnami/metallb \
    -f /vagrant/helm/values-metallb.yaml \
    --create-namespace --namespace metallb-system

echo "[TASK 03] Install of upgrade nginx ingress controller."
helm upgrade --install required-02 bitnami/nginx-ingress-controller \
    --create-namespace --namespace ingress-nginx
echo ""
echo ""
NGINX_IP=""
while [ "${NGINX_IP}x" = "x" ]; do
    sleep 1
    NGINX_IP="$(kubectl get svc -n ingress-nginx | grep Load | awk '{print $4}')"
done
echo "NGINX Ingress Controller listening on $NGINX_IP"
echo "http://$NGINX_IP"   

